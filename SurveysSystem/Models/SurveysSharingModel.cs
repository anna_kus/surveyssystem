﻿using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveysSystem.Models
{
    public class SurveysSharingModel
    {
        //wyswietlanie
        [Editable(false)]
        public string Title { get; set; }

        [HiddenInput(DisplayValue = false)]
        public List<SurveyElement> Elements { get; set; }

        [Editable(false)]
        public List<Content> Contents { get; set; }

        [Editable(false)]
        public Dictionary<int, List<string>> QuestionAnswer { get; set; }

        public Dictionary<int, List<string>> OneChoiceAnswer { get; set; }

        public List<CheckBoxList> CheckedList { get; set; }

        //do pytan otwartych
        public List<int> OpenQuestions { get; set; }

        //moze byc puste - jesli uzytk nie udzieli odpowiedzi
        public string TextAnswer { get; set; }
    }

    public class CheckBoxList
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }
    }
}