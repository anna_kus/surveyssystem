﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveysSystem.Models
{
    public class DeletingSurveyModel
    {
        [Display(Name = "Wybierz ankietę do usunięcia")]
        public int ? SelectedTitle { get; set; }

        public SelectList SurveyTitleToChoose { get; set; }

        [Display(Name = "Wybierz pytanie do usunięcia")]
        public int ? SelectedContent { get; set; }

        public SelectList ContentToChoose { get; set; }

    }
}