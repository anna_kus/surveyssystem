﻿using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SurveysSystem.Models
{
    public class ElementsModel
    {
        [Required(ErrorMessage = "Uzupełnij treść polecenia")]
        [Display(Name = "Uzupełnij treść polecenia")]
        public Content Question { get; set; }

        [Required(ErrorMessage = "Wpisz ilość odpowiedzi")]
        [Display(Name = "Wybierz ilość odpowiedzi")]
        [Range(1, 5, ErrorMessage = "Wybierz wartość z zakresu od 1 do 5")]
        public int Amount_of_answers { get; set; }

        [Required(ErrorMessage = "Dodaj możliwe odpowiedzi")]
        [Display(Name = "Dodaj możliwe odpowiedzi")]
        public List<Content> Answer { get; set; }
    }

  
}