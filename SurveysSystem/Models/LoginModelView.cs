﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SurveysSystem.Models
{
    public class LoginModelView
    {
        [Required(ErrorMessage = "Wpisz swój adres email", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "Wpisz poprawny adres email")]
        [Display(Name = "Adres email")]

        public string email { get; set; }

        [Required(ErrorMessage = "Wpisz hasło", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Hasło musi mieć od 6 do 50 znaków")]
        [Display(Name = "Hasło")]
        public string password { get; set; }
    }
}