﻿using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveysSystem.Models
{
    public class SurveysModel
    { 
        [Required(ErrorMessage = "Wybierz tytuł ankiety")]
        [Display(Name = "Wybierz tytuł ankiety spośród dostępnych w bazie...")]
        public int SelectedTitle { get; set; }

        public SelectList SurveyTitleToChoose { get; set; }

        [Display(Name = "...lub dodaj nowy tytuł")]
        public Content SurveyTitleTextBox { get; set; }

        [Required(ErrorMessage = "Wybierz treść polecenia")]
        [Display(Name = "Wybierz treść polecenia")]
        public int SelectedQuestion { get; set; }

        public SelectList QuestionsToChoose { get; set; }

        [Required(ErrorMessage = "Wybierz typ")]
        [Display(Name = "Wybierz z listy typ pytania")]
        public int SelectedType { get; set; }

        public SelectList AnswerType { get; set; }

        [Required(ErrorMessage = "Wpisz ilość odpowiedzi")]
        [Display(Name = "Wybierz ilość odpowiedzi")]
        [Range(0, 5, ErrorMessage="Wybierz wartość z zakresu od 0 do 5")]
        public int Amount_of_answers { get; set; }

        [Required(ErrorMessage = "Wybierz możliwe odpowiedzi")]
        [Display(Name = "Wbierz możliwe odpowiedzi")]
        public int SelectedAnswer { get; set; }

        public SelectList AnswersToChoose { get; set; }
        
    }
}