﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveysSystem.Models
{
    public class SurveysSelecting
    {
        [Required(ErrorMessage = "Wybierz ankietę")]
        [Display(Name = "Wybierz ankietę spośród dostępnych w bazie...")]
        public int SelectedSurvey{ get; set; }

        public SelectList SurveysToChoose { get; set; }
    }
}