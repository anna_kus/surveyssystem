﻿using SurveysSystem.Models;
using SurveysSystem_Model.Abstract;
using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace SurveysSystem.Controllers
{
    [Authorize]
    public class SharingController : Controller
    {
        private IRepositories repository;

        public SharingController(IRepositories repo)
        {
            repository = repo;
        }

        [HttpGet]
        public ActionResult SurveysChoosing()
        {
            SurveysSelecting survey = new SurveysSelecting();

            List<Content> contents = repository.Contents.ToList();
            List<Survey> surveys = repository.Surveys.ToList();
            List<Content> surveyTitles = new List<Content>();

            List<Survey> surveysWithElements = new List<Survey>();

            foreach (var item in surveys)
            {
                if (repository.SurveyElements.Any(e => e.survey_id == item.id))
                {
                    surveysWithElements.Add(item);
                }
            }

            foreach (var item in contents)
            {
                Content title = new Content();

                if (surveysWithElements.Any(e => e.content_id == item.id))
                {
                    title.text = item.text;
                    title.id = repository.Surveys.First(e => e.content_id == item.id).id;

                    surveyTitles.Add(title);
                }
            }

            survey.SurveysToChoose = new SelectList(surveyTitles, "id", "text");

            return View(survey);
        }

        [HttpPost]
        public ActionResult SurveysChoosing(SurveysSelecting survey)
        {
            List<Content> contents = repository.Contents.ToList();
            List<Survey> surveys = repository.Surveys.ToList();
            List<Content> surveyTitles = new List<Content>();

            List<Survey> surveysWithElements = new List<Survey>();

            foreach (var item in surveys)
            {
                if (repository.SurveyElements.Any(e=>e.survey_id == item.id))
                {
                    surveysWithElements.Add(item);
                }
            }

            foreach (var item in contents)
            {
                Content title = new Content();

                if (surveysWithElements.Any(e => e.content_id == item.id))
                {
                    title.text = item.text;
                    title.id = repository.Surveys.First(e => e.content_id == item.id).id;

                    surveyTitles.Add(title);
                }
            }

            survey.SurveysToChoose = new SelectList(surveyTitles, "id", "text");

            TempData["ID"] = survey.SelectedSurvey;

            
            UsersAnswers(survey.SelectedSurvey);

            return View("UsersAnswers");
        }

       

        [HttpGet]
        public ActionResult UsersAnswers(int id)
        {
            SurveysSharingModel sharingModel = new SurveysSharingModel();

            int title_content_id = repository.Surveys.First(e=>e.id == id).content_id;
            sharingModel.Title  = repository.Contents.First(e=>e.id == title_content_id).text;
           
            #region wydobywanie elementow z ankiety 
            List<SurveyElement> elements = new List<SurveyElement>();

            foreach (var element in repository.SurveyElements)
            {
                if (element.survey_id == id)
                {
                    elements.Add(element);
                }
            }

            sharingModel.Elements = elements;
            #endregion


            #region wydobywanie pytan i ich typow z bazy
            List<int> question_ids = new List<int>();
            List<int> question_content_ids = new List<int>();
            Dictionary<int, int> questions = new Dictionary<int, int>();


            List<string> question_contents = new List<string>();

            List<int> one_choice_questions = new List<int>();
            List<int> multi_choice_questions = new List<int>();
            List<int> open_questions = new List<int>();

            //uzupelnianie pytan
            foreach (var item in repository.SurveyElements)
            {
                if (item.survey_id == id)
                {
                    question_content_ids.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 1)
                {
                    one_choice_questions.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 2)
                {
                    multi_choice_questions.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 3)
                {
                    open_questions.Add(item.question_id);
                }
            }

            //przypisanie content id dla odpowiedniego pytania
            foreach (var item in elements)
            {
                if (questions.Any(e=>e.Key == item.question_id))
                {
                    item.question_id = questions.First(e => e.Key == item.question_id).Value;
                }
            }

            #endregion

            #region wydobywanie odpowiedzi z bazy
            List<int> answer_ids = new List<int>();
            List<string> answer_content_ids = new List<string>();

            foreach (var item in repository.SurveyElements)
            {
                if (item.survey_id == id)
                {
                    answer_ids.Add(item.answer_set_id);
                }
            }

            foreach (var item in answer_ids)
            {
                if (repository.AnswerSets.Any(e => e.id == item))
                {
                    answer_content_ids.Add(repository.AnswerSets.First(e => e.id == item).content_id);
                }
            }

            string[] answerSplit;
            List<string[]> answerSplitList = new List<string[]>();
            List<int> answer_content_ids_all = new List<int>();

            foreach (var item in answer_content_ids)
            {
                answerSplit = item.Split(';');
                answerSplitList.Add(answerSplit);
            }

            List<Content> contents = repository.Contents.ToList();

            Dictionary<int, List<string>> elements_content = new Dictionary<int, List<string>>();

            for (int i = 0; i < question_content_ids.Count(); i++)
            {
                List<string> answers = new List<string>();
                for (int j = 0; j < answerSplitList[i].Count(); j++)
                {
                    if (contents.Any(e => e.id.ToString() == answerSplitList[i][j]))
                    {
                        answers.Add(contents.First(e => e.id.ToString() == answerSplitList[i][j]).text);
                        answer_content_ids_all.Add(Convert.ToInt32(answerSplitList[i][j]));
                    }
                }

                elements_content.Add(question_content_ids[i], answers);
            }
            #endregion

            #region wydobywanie odpowiednich tekstow z Content

            answer_content_ids_all.AddRange(question_content_ids);

            List<Content> contents_in_element = new List<SurveysSystem_Model.Entities.Content>();

            foreach (var item in answer_content_ids_all)
            {
                if (repository.Contents.Any(e=>e.id == item))
                {
                    contents_in_element.Add(repository.Contents.First(e => e.id == item));
                }
            }

            sharingModel.Contents = contents_in_element;

            #endregion

            List<CheckBoxList> check_list = new List<CheckBoxList>();

            sharingModel.QuestionAnswer = elements_content;

            foreach (var item in elements_content)
            {
                for (int i = 0; i < item.Value.Count(); i++)
                {
                    if (multi_choice_questions.Any(e=>e == item.Key))
                    {
                        check_list.Add(new CheckBoxList { Id = item.Key, Value = item.Value[i], IsSelected = true });
                    }
                }
            }

            sharingModel.CheckedList = check_list;

            Dictionary<int, List<string>> one_choice_question_list = new Dictionary<int, List<string>>();
            List<int> open_question_list = new List<int>();

            foreach (var item in elements_content)
            {
                if (one_choice_questions.Any(e => e == item.Key))
                {
                    one_choice_question_list.Add(item.Key, item.Value);
                }

                if (open_questions.Any(e => e == item.Key))
                {
                    open_question_list.Add(item.Key);
                }
            }

            sharingModel.OpenQuestions = open_question_list;
            sharingModel.OneChoiceAnswer = one_choice_question_list;

            return View(sharingModel);
        }

        [HttpPost]
        public ActionResult UsersAnswers(SurveysSharingModel sharingModel)
        {
            int id = Convert.ToInt32(TempData["ID"]);

            #region ponowne uzupelnianie danych z bazy
            int title_content_id = repository.Surveys.First(e => e.id == id).content_id;
            sharingModel.Title = repository.Contents.First(e => e.id == title_content_id).text;

            #region wydobywanie elementow z ankiety
            List<SurveyElement> elements = new List<SurveyElement>();

            foreach (var element in repository.SurveyElements)
            {
                if (element.survey_id == id)
                {
                    elements.Add(element);
                }
            }

            sharingModel.Elements = elements;
            #endregion


            #region wydobywanie pytan i ich typow z bazy
            List<int> question_ids = new List<int>();
            List<int> question_content_ids = new List<int>();
            Dictionary<int, int> questions = new Dictionary<int, int>();

            List<string> question_contents = new List<string>();

            List<int> one_choice_questions = new List<int>();
            List<int> multi_choice_questions = new List<int>();
            List<int> open_questions = new List<int>();


            //uzupelnianie pytan
            foreach (var item in repository.SurveyElements)
            {
                if (item.survey_id == id)
                {
                    question_content_ids.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 1)
                {
                    one_choice_questions.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 2)
                {
                    multi_choice_questions.Add(item.question_id);
                }

                if (item.survey_id == id && item.type_id == 3)
                {
                    open_questions.Add(item.question_id);
                }
            }
            #endregion

            #region wydobywanie odpowiedzi z bazy
            List<int> answer_ids = new List<int>();
            List<string> answer_content_ids = new List<string>();

            foreach (var item in repository.SurveyElements)
            {
                if (item.survey_id == id)
                {
                    answer_ids.Add(item.answer_set_id);
                }
            }

            foreach (var item in answer_ids)
            {
                if (repository.AnswerSets.Any(e => e.id == item))
                {
                    answer_content_ids.Add(repository.AnswerSets.First(e => e.id == item).content_id);
                }
            }

            string[] answerSplit;
            List<string[]> answerSplitList = new List<string[]>();
            List<int> answer_content_ids_all = new List<int>();

            foreach (var item in answer_content_ids)
            {
                answerSplit = item.Split(';');
                answerSplitList.Add(answerSplit);
            }

            List<Content> contents = repository.Contents.ToList();

            //lista elementow - pytanie + odpowiedzi
            Dictionary<int, List<string>> elements_content = new Dictionary<int, List<string>>();

            for (int i = 0; i < question_content_ids.Count(); i++)
            {
                List<string> answers = new List<string>();
                for (int j = 0; j < answerSplitList[i].Count(); j++)
                {
                    if (contents.Any(e => e.id.ToString() == answerSplitList[i][j]))
                    {
                        answers.Add(contents.First(e => e.id.ToString() == answerSplitList[i][j]).text);
                        answer_content_ids_all.Add(Convert.ToInt32(answerSplitList[i][j]));
                    }
                }

                elements_content.Add(question_content_ids[i], answers);
            }
            #endregion

            #region wydobywanie odpowiednich tekstow z Content

            answer_content_ids_all.AddRange(question_content_ids);

            List<Content> contents_in_element = new List<SurveysSystem_Model.Entities.Content>();

            foreach (var item in answer_content_ids_all)
            {
                if (repository.Contents.Any(e => e.id == item))
                {
                    contents_in_element.Add(repository.Contents.First(e => e.id == item));
                }
            }

            sharingModel.Contents = contents_in_element;

            #endregion


            sharingModel.QuestionAnswer = elements_content;

            #endregion

            Dictionary<int, List<string>> one_choice_question_list = new Dictionary<int, List<string>>();
            List<CheckBoxList> check_list = new List<CheckBoxList>();
            List<int> open_question_list = new List<int>();

            foreach (var item in elements_content)
            {
                if (one_choice_questions.Any(e => e == item.Key))
                {
                    one_choice_question_list.Add(item.Key, item.Value);
                }

                if (open_questions.Any(e => e == item.Key))
                {
                    open_question_list.Add(item.Key);
                }

                for (int i = 0; i < item.Value.Count(); i++)
                {
                    if (multi_choice_questions.Any(e => e == item.Key))
                    {
                        check_list.Add(new CheckBoxList { Id = item.Key, Value = item.Value[i], IsSelected = true });
                    } 
                }
            }

            sharingModel.CheckedList = check_list;
            sharingModel.OpenQuestions = open_question_list;
            sharingModel.OneChoiceAnswer = one_choice_question_list;

            #region zapisywanie odpowiedzi uzytkownika

            if (ModelState.IsValid)
            {
                foreach (var item in sharingModel.OneChoiceAnswer)
                {
                    UserAnswer user_answer = new UserAnswer();
                    int selected_answer = 0;
                    string asnw_txt = Request.Form[item.Key.ToString()];
                    selected_answer = repository.Contents.First(e => e.text == asnw_txt).id;
                    user_answer.survey_id = id;
                    user_answer.user_id = repository.Users.First(u=>u.email== User.Identity.Name).id;
                    user_answer.survey_element_id = sharingModel.Elements.First(e=>e.question_id == item.Key).id;
                    user_answer.user_answer_id = selected_answer;

                    if (repository.SaveUserAnswers(user_answer) == "")
                    {
                        TempData["message"] = "Zapisano odpowiedź.";
                    }
                }
  
                foreach (var item in sharingModel.CheckedList)
                {
                    UserAnswer user_answer = new UserAnswer();

                    int selected_answer = 0;

                    if (item.IsSelected)
                    {
                        string a = item.Value;
                        selected_answer = repository.Contents.First(e => e.text == a).id;
                    }

                    user_answer.survey_id = id;
                    user_answer.user_id = repository.Users.First(u => u.email == User.Identity.Name).id;
                    user_answer.survey_element_id = sharingModel.Elements.First(e => e.question_id == item.Id).id;
                    user_answer.user_answer_id = selected_answer;

                    if (repository.SaveUserAnswers(user_answer) == "")
                    {
                        TempData["message"] = "Zapisano odpowiedź.";
                    }
                }

                foreach (var item in sharingModel.OpenQuestions)
                {
                    UserAnswer user_answer = new UserAnswer();
                    int selected_answer = 65;

                    if (sharingModel.TextAnswer!=null || sharingModel.TextAnswer!="")
                    {
                        user_answer.text_answer = sharingModel.TextAnswer;  
                    }
                    else
                    {
                        user_answer.text_answer = "Brak odpowiedzi";
                    }

                    user_answer.survey_id = id;
                    user_answer.user_id = repository.Users.First(u => u.email == User.Identity.Name).id;
                    user_answer.survey_element_id = sharingModel.Elements.First(e => e.question_id == item).id;
                    user_answer.user_answer_id = selected_answer;

                    if (repository.SaveUserAnswers(user_answer) == "")
                    {
                        TempData["message"] = "Zapisano odpowiedź.";
                    }
                }

                #region export danych do pliku excel
                //odpowiedzi
                var answer_to_export = repository.UserAnswers.ToList();
                System.Web.UI.WebControls.GridView answers_gridview = new System.Web.UI.WebControls.GridView();
                answers_gridview.DataSource = answer_to_export;
                answers_gridview.DataBind();
                Response.ClearContent();
                Response.Buffer = true;        
                Response.AddHeader("content-disposition", "attachment; filename=users_answers.xls"); Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        answers_gridview.RenderControl(htw);
                        Response.Output.Write(sw.ToString());  
                        Response.Flush();
                        Response.End();
                    }
                }
               
                #endregion

                return View(sharingModel);
            }
            else
            {
                
                TempData["message"] = string.Format("Coś nie tak.");
                return View(sharingModel);
            }

            #endregion
        }

    }
}
