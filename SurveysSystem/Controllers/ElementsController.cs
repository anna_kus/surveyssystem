﻿using SurveysSystem.Models;
using SurveysSystem_Model.Abstract;
using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveysSystem.Controllers
{
    [Authorize]
    public class ElementsController : Controller
    {
        //obiekt reprezentujący repozytorium danych
        private IRepositories repository;

        public ElementsController(IRepositories repo)
        {
            repository = repo;
        }

        #region dodawanie pytań

        [HttpGet]
        public ActionResult Edit()
        {
            //tworzenie nowego obiektu modelu widoku ElementsModel
            //w celu uzupełnienia początkowo wyświetlanych danych
            ElementsModel element = new ElementsModel(); 
        
            //tworzenie listy wyboru ilości odpowiedzi
            List<int> numbers = new List<int>();
            for (int i = 1; i < 6; i++)
            {
                numbers.Add(i);
            }
            SelectList numberList = new SelectList(numbers);
            ViewBag.Numbers = numberList;
            return View(element);
        }

        [HttpPost]
        public ActionResult Edit(ElementsModel element, string buttonType)
        {
            //wiadomości przekazywane do widoku
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";
            string messageError2 = "Pole nie może być puste.";

            //tworzenie nowych obiektow potrzebych typow
            Question question = new Question();
            AnswerSet answerSet = new AnswerSet();

            //tworzenie listy wyboru ilości odpowiedzi
            List<int> numbers = new List<int>();
            for (int i = 1; i < 6; i++)
            {
                numbers.Add(i);
            }
            SelectList numberList = new SelectList(numbers);
            ViewBag.Numbers = numberList;

            if (buttonType == "Zapisz")
            {
                //sprawdzanie poprawnosci modelu w celu zapisu danych do bazy
                if (ModelState.IsValid)
                {
                    //jezeli metoda zapisu tresci do bazy zwróciła wartość
                    //świadczącą o powodzeniu,
                    //następudo dodanie do bazy kolejnego elementu
                    string questionSuccess = repository.SaveContent(element.Question);

                    if (questionSuccess == messageOk)
                    {
                        //dodanie pytania do bazy
                        //gdzie wartoscia jest identyfikator tresci
                        question.content_id = element.Question.id;
                        repository.SaveQuestion(question);
                    }
                    //procedura w razie błęu
                    else if (questionSuccess == messageError1)
                    {
                        TempData["questionMessage"] = string.Format(messageError1 + " takie pytanie.");
                    }
                    else if (questionSuccess == messageError2)
                    {
                        TempData["questionMessage"] = string.Format(messageError2);
                    }

                    //zapamietanie wybranej ilosci odpowiedzi
                    if (element.Amount_of_answers >= 0)
                    {
                        ViewBag.AnswersAmount = element.Amount_of_answers;
                    }
                    
                    //procedura w razie błęu
                    else
                    {
                        TempData["amountMessage"] = string.Format(messageError2);
                    }

                    
                        //dodawanie do bazy zestawów odpowiedzi
                        //są one dodawane w postaci zbioru identyfiaktórw 
                        //odpowiednich treści oddzielonych średnikiem, 
                        //np: 62;63;65
                        List<int> IDList = new List<int>();
                        string answerSuccess = messageError2;

                        for (int i = 0; i < element.Amount_of_answers; i++)
                        {
                            //zapis kolejnych tresci
                            answerSuccess = repository.SaveContent(element.Answer[i]);

                            if (answerSuccess == messageOk)
                            {
                                IDList.Add(element.Answer[i].id);
                            }
                            else if (answerSuccess == messageError1)
                            {
                                TempData["answerMessage"] = string.Format(messageError1 + " taka odpowiedź.");
                            }
                            else if (answerSuccess == messageError2)
                            {
                                TempData["answerMessage"] = string.Format(messageError2);
                            }
                        }

                        if (IDList.Count > 0)
                        {
                            //tworzenie odpowiedniej formy zestawu odpowiedzi
                            foreach (var item in IDList)
                            {
                                answerSet.content_id += item.ToString() + ';';
                            }
                            //zapis zestawu odpowiedzi do bazy
                            repository.SaveAnswers(answerSet);
                        }
                    
                    //informacja w razie sukcesu
                    TempData["message"] = string.Format("Zapisano nowy element ankiety.");
                    ModelState.Clear();
                   
                }
                else
                {
                    //informacja w razie błedu
                    TempData["message"] = string.Format("Coś nie tak.");
                    ModelState.Clear();
                }
                return View(element);
            }

            if (buttonType == "Nowe pytanie")
            {
                return View(new ElementsModel());
            }

            return View(element);
        }
        #endregion

#region ukladanie ankiety

        [HttpGet]
        public ActionResult CreateSurvey()
        {
            #region wyswietlanie danych w drop down listach
            SurveysModel surveyModel = new SurveysModel();
            surveyModel.AnswerType = new SelectList(repository.Types, "id", "type");
            
            List<Content> contents = repository.Contents.ToList();

            List<Content> surveyTitles = new List<Content>();

            foreach (var item in contents)
            {
                if (repository.Surveys.Any(e => e.content_id == item.id))
                {
                    surveyTitles.Add(item);
                }
            }

            surveyModel.SurveyTitleToChoose = new SelectList(surveyTitles, "id", "text");

            List<Content> questions = new List<Content>();

            foreach (var item in contents)
            {
                if (repository.Questions.Any(e=>e.content_id==item.id))
                {
                    questions.Add(item);
                }
            }

            surveyModel.QuestionsToChoose = new SelectList(questions, "id", "text");

            List<Content> answers = new List<SurveysSystem_Model.Entities.Content>();

            Dictionary<int, string[]> answerSplitList = new Dictionary<int, string[]>();

            string[] answerSplit;

            string answerElement = "";


            List<AnswerSet> lista = new List<AnswerSet>();
            lista = repository.AnswerSets.ToList();

            foreach (var item in repository.AnswerSets)
            {
                answerSplit = item.content_id.Split(';');
                answerSplitList.Add(item.id, answerSplit);
            }

            var enumerator = answerSplitList.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var pair = enumerator.Current;
               

                for (int i = 0; i < pair.Value.Count(); i++)
                {
                    if (contents.Any(e => e.id.ToString() == pair.Value[i]))
                    {
                        answerElement += contents.First(e => e.id.ToString() == pair.Value[i]).text + ';';
                    }
                }
                Content answCont = new SurveysSystem_Model.Entities.Content(); //!!!!!!! tutaj trzeba bylo zadeklarowac zmienna
                answCont.id = pair.Key;
                answCont.text = answerElement;
                answers.Add(answCont);
                answerElement = "";

            }

            surveyModel.AnswersToChoose = new SelectList(answers, "id", "text");
            #endregion

            return View(surveyModel);
        }

        [HttpPost]
        public ActionResult CreateSurvey(SurveysModel surveyModel, string buttonType)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";
            string messageError2 = "Wybierz element z listy.";

            Question question = new Question();
            AnswerSet answerSet = new AnswerSet();
            Survey survey = new Survey();
            SurveyElement surveyElement = new SurveyElement();

            #region uzupelnianie drop down list
            List<Content> contents = repository.Contents.ToList();

            List<Content> surveyTitles = new List<Content>();

            foreach (var item in contents)
            {
                if (repository.Surveys.Any(e => e.content_id == item.id))
                {
                    surveyTitles.Add(item);
                }
            }

            surveyModel.SurveyTitleToChoose = new SelectList(surveyTitles, "id", "text");

            List<Content> questions = new List<Content>();

            foreach (var item in contents)
            {
                if (repository.Questions.Any(e => e.content_id == item.id))
                {
                    questions.Add(item);
                }
            }

            surveyModel.QuestionsToChoose = new SelectList(questions, "id", "text");

            surveyModel.AnswerType = new SelectList(repository.Types, "id", "type");


            List<Content> answers = new List<SurveysSystem_Model.Entities.Content>();

            Dictionary<int, string[]> answerSplitList = new Dictionary<int, string[]>();

            string[] answerSplit;

            string answerElement = "";


            List<AnswerSet> lista = new List<AnswerSet>();
            lista = repository.AnswerSets.ToList();

            foreach (var item in repository.AnswerSets)
            {
                answerSplit = item.content_id.Split(';');
                answerSplitList.Add(item.id, answerSplit);
            }

            var enumerator = answerSplitList.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var pair = enumerator.Current;


                for (int i = 0; i < pair.Value.Count(); i++)
                {
                    if (contents.Any(e => e.id.ToString() == pair.Value[i]))
                    {
                        answerElement += contents.First(e => e.id.ToString() == pair.Value[i]).text + ';';
                    }
                }
                Content answCont = new SurveysSystem_Model.Entities.Content(); //!!!!!!! tutaj trzeba bylo zadeklarowac zmienna
                answCont.id = pair.Key;
                answCont.text = answerElement;
                answers.Add(answCont);
                answerElement = "";

            }

            surveyModel.AnswersToChoose = new SelectList(answers, "id", "text");

            #endregion

            #region dodawanie nowego elementu do bazy

            if (buttonType == "Zapisz")
            {
                if (ModelState.IsValid)
                {
                    if (surveyModel.SurveyTitleTextBox.text != String.Empty) //?? czy string.empty?
                    {
                        string titleSuccess = repository.SaveContent(surveyModel.SurveyTitleTextBox);

                        if (titleSuccess == messageOk)
                        {
                            surveyModel.SelectedTitle = repository.Contents.Where(x => x.text == surveyModel.SurveyTitleTextBox.text).Single().id;
                            survey.content_id = surveyModel.SelectedTitle;
                            repository.SaveSurvey(survey);
                        }

                        else if (titleSuccess == messageError1)
                        {
                            TempData["surveyMessage"] = string.Format(messageError1 + " taki tytuł ankiety.");
                        }

                        else if (titleSuccess == messageError2)
                        {
                            TempData["surveyMessage"] = string.Format(messageError2);
                        }
                    }

                    if (surveyModel.SelectedTitle > 0)
                    {
                        surveyElement.survey_id = repository.GetElelemtId("Surveys", surveyModel.SelectedTitle);
                    }
                    else
                    {
                        TempData["surveyMessage"] = string.Format(messageError2);
                    }


                    if (surveyModel.SelectedQuestion > 0)
                    {
                        surveyElement.question_id = surveyModel.SelectedQuestion;// repository.GetElelemtId("Questions", surveyModel.SelectedQuestion);
                    }
                    else
                    {
                        TempData["questionMessage"] = string.Format(messageError2);
                    }

                    if (surveyModel.SelectedAnswer > 0)
                    {
                        surveyElement.answer_set_id = surveyModel.SelectedAnswer;
                    }
                    else
                    {
                        TempData["answerMessage"] = string.Format(messageError2);
                    }

                    if (surveyModel.Amount_of_answers >= 0)
                    {
                        surveyElement.amount_of_answers = surveyModel.Amount_of_answers;
                    }
                    else
                    {
                        TempData["amountMessage"] = string.Format(messageError2);
                    }


                    if (surveyModel.SelectedType == 1 || surveyModel.SelectedType == 2 || surveyModel.SelectedType == 3)
                    {
                        surveyElement.type_id = surveyModel.SelectedType;
                    }
                    else
                    {
                        TempData["selectedType"] = string.Format(messageError2);
                    }

                    TempData["message"] = repository.SaveElement(surveyElement);
                    ModelState.Clear();
                    return View(surveyModel);
                }
                else
                {
                    TempData["message"] = string.Format("Coś nie tak.");
                    ModelState.Clear();
                    return View(surveyModel);
                }
            }

            if (buttonType=="Dodaj pytanie")
            {
                SurveysModel newModel = new SurveysModel();
                
                newModel.AnswersToChoose = surveyModel.AnswersToChoose;
                newModel.AnswerType = surveyModel.AnswerType;
                newModel.QuestionsToChoose = surveyModel.QuestionsToChoose;
                newModel.SelectedTitle = surveyModel.SelectedTitle;
                newModel.SurveyTitleTextBox = surveyModel.SurveyTitleTextBox;
                newModel.SurveyTitleToChoose = surveyModel.SurveyTitleToChoose;

                return View(newModel);
            }
            return View(surveyModel);
            #endregion
        }
#endregion

        #region usuwanie elementów ankiety

        [HttpGet]
        public ActionResult DeleteSurvey()
        {
            #region wyswietlanie danych w drop down listach
            DeletingSurveyModel deletingModel = new DeletingSurveyModel();

            List<Content> contentsAll = repository.Contents.ToList();

            List<Content> surveyTitles = new List<Content>();

            List<Content> contentsToDelete = new List<Content>();

            foreach (var item in contentsAll)
            {
                if (item.id != 65)
                {
                    contentsToDelete.Add(item);
                }
            }

            foreach (var item in contentsAll)
            {
                if (repository.Surveys.Any(e => e.content_id == item.id))
                {
                    surveyTitles.Add(item);
                }
            }

            deletingModel.SurveyTitleToChoose = new SelectList(surveyTitles, "id", "text");

            deletingModel.ContentToChoose = new SelectList(contentsToDelete, "id", "text");

            
            #endregion

            return View(deletingModel);
        }

        [HttpPost]
        public ActionResult DeleteSurvey(DeletingSurveyModel deletingSurveyModel)
        {
            //string messageOk = "";
           // string messageError1 = "W bazie istnieje już";
            string messageError2 = "Wybierz element z listy.";

            #region uzupelnianie drop down list
            List<Content> contentsAll = repository.Contents.ToList();

            List<Content> surveyTitles = new List<Content>();

            List<Content> contentsToDelete = new List<Content>();

            foreach (var item in contentsAll)
            {
                if (item.id != 65)
                {
                    contentsToDelete.Add(item);
                }
            }

            foreach (var item in contentsAll)
            {
                if (repository.Surveys.Any(e => e.content_id == item.id))
                {
                    surveyTitles.Add(item);
                }
            }

            deletingSurveyModel.SurveyTitleToChoose = new SelectList(surveyTitles, "id", "text");

            deletingSurveyModel.ContentToChoose = new SelectList(contentsToDelete, "id", "text");

            #endregion
            #region usuwanie elementow z bazy

            if (deletingSurveyModel.SelectedTitle > 0)
            {
                if (repository.Surveys.Any(s=>s.content_id == deletingSurveyModel.SelectedTitle))
                {
                    Survey survey = repository.Surveys.First(s => s.content_id == deletingSurveyModel.SelectedTitle);
                    TempData["surveyMessage"] = repository.DeleteSurvey(survey);
                }
                
            }
            //else
            //{
            //    TempData["surveyMessage"] = string.Format(messageError2);
            //}


            if (deletingSurveyModel.SelectedContent> 0)
            {
                Content content = repository.Contents.First(c => c.id == deletingSurveyModel.SelectedContent);
                TempData["contentMessage"] = repository.DeleteContent(content);
            }
            //else
            //{
            //    TempData["contentMessage"] = string.Format(messageError2);
            //}

           // TempData["message"] = repository.SaveElement(surveyElement);

            return View(deletingSurveyModel);

            #endregion
        }
        #endregion
    }
}
