﻿using Ninject;
using SurveysSystem_Model.Abstract;
using SurveysSystem_Model.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace SurveysSystem.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext context, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            //powiązanie pomiędzy interfejsu repozytorium z rzeczywistym repozytorium bazy danych
            ninjectKernel.Bind<IRepositories>().To<EFSurveyRepository>();
        }
    }
}