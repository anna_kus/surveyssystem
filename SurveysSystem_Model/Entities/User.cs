﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SurveysSystem_Model.Entities
{
    public class User
    {
        [HiddenInput(DisplayValue=false)]
        public int id { get; set; }

        [Required(ErrorMessage = "Wpisz swoje imię lub nick", AllowEmptyStrings = false)]
        [Display(Name="Imię")]
        public string name { get; set; }

        [Required(ErrorMessage = "Wpisz swoje nazwisko", AllowEmptyStrings = false)]
        [Display(Name = "Nazwisko")]
        public string last_name { get; set; }

        [Required(ErrorMessage = "Wpisz swój adres email", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage="Wpisz poprawny adres email")]
        [Display(Name = "Adres email")]

        public string email { get; set; }

        [Required(ErrorMessage = "Wpisz hasło", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [StringLength(150, MinimumLength = 6, ErrorMessage = "Hasło musi mieć od 6 do 50 znaków")]
        [Display(Name = "Hasło")]
        public string password { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string password_salt { get; set; }

        //co jeszczze?
        //czy pozycja - ankieter/ankietowany? - może być tym i tym, może nadawanie roli i uprawnien?
    }
}
