﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveysSystem_Model.Entities
{
    public class UserAnswer
    {
        public int id { get; set; }
        public int survey_id { get; set; }
        public int user_id { get; set; }
        public int survey_element_id { get; set; }
        public int user_answer_id { get; set; }
        public string text_answer { get; set; }
    }
}
