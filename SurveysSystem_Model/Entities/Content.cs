﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SurveysSystem_Model.Entities
{
    public class Content
    {
        [HiddenInput(DisplayValue = false)]
        public int id { get; set; }

        [Display(Name = "")]
        public string text { get; set; }
    }
}
