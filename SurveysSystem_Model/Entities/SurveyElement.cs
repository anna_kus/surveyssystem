﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveysSystem_Model.Entities
{
    public class SurveyElement
    {
        public int id { get; set; }
        public int survey_id { get; set; }
        public int question_id { get; set; }
        public int type_id { get; set; }
        public int amount_of_answers { get; set; }
        public int answer_set_id { get; set; }
    }
}
