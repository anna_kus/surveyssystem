﻿using SurveysSystem_Model.Abstract;
using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveysSystem_Model.Concrete
{
    public class EFSurveyRepository : IRepositories
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Content> Contents
        {
            get { return context.Contents; }
        }

        public IQueryable<Question> Questions
        {
            get { return context.Questions; }
        }

        public IQueryable<AnswerSet> AnswerSets
        {
            get { return context.AnswerSets; }
        }

        public IQueryable<SurveyElement> SurveyElements
        {
            get { return context.SurveyElements; }
        }

        public IQueryable<Survey> Surveys
        {
            get { return context.Surveys; }
        }

        public IQueryable<SurveysSystem_Model.Entities.Type> Types
        {
            get { return context.Types; }
        }

        public IQueryable<User> Users
        {
            get { return context.Users; }
        }

        public IQueryable<UserAnswer> UserAnswers
        {
            get { return context.UserAnswers; }
        }
         
        #region metody do zapisu danych w bazie

        public string SaveContent(Content content)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";
            string messageError2 = "Pole nie może być puste.";

            try
            {
                if (content.text != null)
                {
                    if (!context.Contents.Any(c => c.text == content.text))
                    {
                        context.Contents.Add(content);
                        context.SaveChanges();
                        return messageOk;
                    }
                    else
                    {
                        throw new Exception(messageError1);
                    }
                }
                else
                {
                    throw new Exception(messageError2);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SaveSurvey(Survey survey)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";

            try
            {
                if (!context.Surveys.Any(s => s.content_id == survey.content_id))
                {
                    context.Surveys.Add(survey);
                    context.SaveChanges();
                    return messageOk;
                }
                else
                {
                    throw new Exception(messageError1);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SaveElement(SurveyElement element)
        {
            string messageOk = "Zapisano nowy element ankiety";
            string messageError1 = "W bazie istnieje już";
            string messageError2 = "Pole nie może być puste.";

            try
            {
                if (element.amount_of_answers >= 0 && element.answer_set_id > 0 &&
                    element.question_id > 0 && element.survey_id > 0 && element.type_id > 0)
                {
                    if (!context.SurveyElements.Any(s => (s.survey_id == element.survey_id &&
                        s.question_id == element.question_id && s.type_id == element.type_id &&
                        s.amount_of_answers == element.amount_of_answers &&
                        s.answer_set_id == element.answer_set_id)))
                    {
                        context.SurveyElements.Add(element);
                        context.SaveChanges();
                        return messageOk;
                    }
                    else
                    {
                        throw new Exception(messageError1);
                    }
                }
                else
                {
                    throw new Exception(messageError2);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SaveQuestion(Question question)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";

            try
            {
                if (!context.Questions.Any(s => s.content_id == question.content_id))
                {
                    context.Questions.Add(question);
                    context.SaveChanges();
                    return messageOk;
                }
                else
                {
                    throw new Exception(messageError1);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SaveAnswers(AnswerSet answerSet)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";

            try
            {
                if (!context.AnswerSets.Any(s => s.content_id == answerSet.content_id))
                {
                    context.AnswerSets.Add(answerSet);
                    context.SaveChanges();
                    return messageOk;
                }
                else
                {
                    throw new Exception(messageError1);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SaveUserAnswers(UserAnswer userAnswers)
        {
            string messageOk = "";
            string messageError1 = "W bazie istnieje już";

            try
            {
                //if (!context.UserAnswers.Any(s => (s.survey_id == userAnswers.survey_id &&
                //        s.survey_element_id == userAnswers.survey_element_id && s.text_answer == userAnswers.text_answer &&
                //        s.user_id == userAnswers.user_id)))
                //{
                //    context.UserAnswers.Add(userAnswers);
                //    context.SaveChanges();
                //    return messageOk;
                //}

                if (userAnswers.id == 0)
                {
                    context.UserAnswers.Add(userAnswers);
                    context.SaveChanges();
                    return messageOk;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }

            
        }

        #endregion

        #region metody do usuwania danych w bazie

        public string DeleteUserAnswer(UserAnswer userAnswer)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";

            try
            {
                context.UserAnswers.Remove(userAnswer);
                context.SaveChanges();
                return messageOk;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string DeleteSurveyElement(SurveyElement element)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";
            
            try
            {
                //usuwanie odpowiedzi dla usuwanego elementu
                if (context.UserAnswers.Any(a => a.survey_element_id == element.id))
                {
                    foreach (var answer in context.UserAnswers.ToList())
                    {
                        if (answer.survey_element_id == element.id)
                        {
                            DeleteUserAnswer(answer);
                        }
                    }
                }
                
                context.SurveyElements.Remove(element);
                context.SaveChanges();
                return messageOk;
               
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }


        public string DeleteSurvey(Survey survey)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";

            try
            {
                //usuwanie elementow usuwanej ankiety
                if (context.SurveyElements.Any(s=>s.survey_id==survey.id))
                {
                    foreach (var item in context.SurveyElements.ToList())
                    {
                        if (item.survey_id == survey.id)
                        {
                            DeleteSurveyElement(item);
                        }
                    }
                }
                

                context.Surveys.Remove(survey);
                context.SaveChanges();
                return messageOk;          
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

       
        public string DeleteAnswerSet(AnswerSet answerSet)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";

            try
            {
                //usuwanie elementu ankiety dla danej odpowiedzi
                foreach (var element in context.SurveyElements.ToList())
                {
                    if (element.answer_set_id == answerSet.id)
                    {
                        DeleteSurveyElement(element);
                    }
                }
               
                context.AnswerSets.Remove(answerSet);
                context.SaveChanges();
                return messageOk;
              
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string DeleteQuestion(Question question)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";

            try
            {
                context.Questions.Remove(question);
                context.SaveChanges();
                return messageOk;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string DeleteContent(Content content)
        {
            string messageOk = "Usunięto element i wszystkie jego zależności";
            string messageError1 = "Taki element nie istnieje";
            //Content content = new Content();

            try
            {
                //usuwanie pytania
                if (context.Questions.Any(q => q.content_id == content.id))
                {
                    DeleteQuestion(context.Questions.First(q => q.content_id == content.id));
                }

                //usuwanie ankiety
                if (context.Surveys.Any(s => s.content_id == content.id))
                {
                    DeleteSurvey(context.Surveys.First(s => s.content_id == content.id));
                }

                //usuwanie elementu
                if (context.SurveyElements.Any(s=>s.question_id == content.id))
                {
                    foreach (var element in context.SurveyElements.ToList())
                    {
                        if (element.question_id == content.id)
                        {
                            DeleteSurveyElement(element);
                        }
                    }
                }
                

                //usuwanie odpowiedzi

                List<Content> answers = new List<Content>();

                Dictionary<int, string[]> answerSplitList = new Dictionary<int, string[]>();

                string[] answerSplit;
                string answerElement = "";

                List<AnswerSet> lista = new List<AnswerSet>();
                lista = context.AnswerSets.ToList();

                foreach (var item in context.AnswerSets.ToList())
                {
                    answerSplit = item.content_id.Split(';');
                    answerSplitList.Add(item.id, answerSplit);
                }

                foreach (var answ_split_list in answerSplitList)
                {
                    if (answ_split_list.Value.Any(a => a == content.id.ToString()))
                    {
                        DeleteAnswerSet(context.AnswerSets.First(a=>a.id == answ_split_list.Key));
                    }
                }

                context.Contents.Remove(content);
                context.SaveChanges();
                return messageOk;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        #endregion

        #region metody do wydobywania danych

        public int GetElelemtId(string table, int id_content)
        {
            int element_id;
            switch (table)
            {
                case "Surveys":
                    element_id = context.Surveys.Where(x => x.content_id == id_content).Single().id;
                    break;
                case "Questions":
                    element_id = context.Questions.Where(x => x.content_id == id_content).Single().id;    
                    break;
                case "AnswerSets":
                    element_id = context.AnswerSets.Where(x => x.content_id == id_content.ToString()).Single().id;      
                    break;
                default:
                    element_id = 0;
                    break;
            }
            
            return element_id;
        }

        #endregion
    }
}
