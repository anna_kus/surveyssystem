﻿using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveysSystem_Model.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Content> Contents { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<AnswerSet> AnswerSets { get; set; }
        public DbSet<SurveyElement> SurveyElements { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveysSystem_Model.Entities.Type> Types { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserAnswer> UserAnswers { get; set; }

        //public EFDbContext()
        //{
        //    var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        //}

        
    }
}
