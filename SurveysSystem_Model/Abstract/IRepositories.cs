﻿using SurveysSystem_Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveysSystem_Model.Abstract
{
    public interface IRepositories
    {
        IQueryable<Content> Contents { get; }
        IQueryable<Question> Questions { get; }
        IQueryable<AnswerSet> AnswerSets { get; }
        IQueryable<SurveyElement> SurveyElements { get; }
        IQueryable<Survey> Surveys { get; }
        IQueryable<SurveysSystem_Model.Entities.Type> Types { get; }
        IQueryable<User> Users { get; }
        IQueryable<UserAnswer> UserAnswers { get; }

        string SaveContent(Content content);
        string SaveSurvey(Survey survey);
        string SaveElement(SurveyElement element);
        string SaveQuestion(Question question);
        string SaveAnswers(AnswerSet answerSet);
        string SaveUserAnswers(UserAnswer userAnswers);

        string DeleteUserAnswer(UserAnswer userAnswer);
        string DeleteSurveyElement(SurveyElement element);
        string DeleteSurvey(Survey survey);
        string DeleteAnswerSet(AnswerSet answerSet);
        string DeleteQuestion(Question question);
        string DeleteContent(Content content);

        int GetElelemtId(string table, int content_id);

        
    }
}
